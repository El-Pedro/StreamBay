-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 20 Octobre 2015 à 23:13
-- Version du serveur: 5.5.44-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `StreamBay`
--

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
  `idF` int(11) NOT NULL AUTO_INCREMENT,
  `titreVO` varchar(50) NOT NULL,
  `titreVF` varchar(50) NOT NULL,
  `dateRea` varchar(4) NOT NULL,
  `realisateur` varchar(50) NOT NULL,
  `note` varchar(2) NOT NULL,
  `description` text NOT NULL,
  `lang` varchar(15) NOT NULL,
  `genre` varchar(20) NOT NULL,
  `dateAjout` varchar(16) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`idF`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `movies`
--

INSERT INTO `movies` (`idF`, `titreVO`, `titreVF`, `dateRea`, `realisateur`, `note`, `description`, `lang`, `genre`, `dateAjout`, `image`) VALUES
(1, 'Django Unchained', 'Django Unchained', '2012', 'Quentin Tarantino', '9', 'Dans le sud des États-Unis, deux ans avant la guerre de Sécession, le Dr King Schultz, un chasseur de primes allemand, fait l’acquisition de Django, un esclave qui peut l’aider à traquer les frères Brittle, les meurtriers qu’il recherche. Schultz promet à Django de lui rendre sa liberté lorsqu’il aura capturé les Brittle – morts ou vifs.\r\nAlors que les deux hommes pistent les dangereux criminels, Django n’oublie pas que son seul but est de retrouver Broomhilda, sa femme, dont il fut séparé à cause du commerce des esclaves…\r\nLorsque Django et Schultz arrivent dans l’immense plantation du puissant Calvin Candie, ils éveillent les soupçons de Stephen, un esclave qui sert Candie et a toute sa confiance. Le moindre de leurs mouvements est désormais épié par une dangereuse organisation de plus en plus proche… Si Django et Schultz veulent espérer s’enfuir avec Broomhilda, ils vont devoir choisir entre l’indépendance et la solidarité, entre le sacrifice et la survie…', 'Anglais', 'Western', '20/10/15 - 19H22', 'DJUN.jpg'),
(9, 'The Wolf of Wall Street', 'Le Loup de Wall Street', '2013', 'Martin Scorsese', '8', 'Lâ€™argent. Le pouvoir. Les femmes. La drogue. Les tentations Ã©taient lÃ , Ã  portÃ©e de main, et les autoritÃ©s nâ€™avaient aucune prise. Aux yeux de Jordan et de sa meute, la modestie Ã©tait devenue complÃ¨tement inutile. Trop nâ€™Ã©tait jamais assezâ€¦', 'Francais', 'Drame', '20/10/15 - 21H34', 'LOUPWS.jpg'),
(12, 'Vice Versa', 'Vice Versa', '2015', 'Pete Docter', '7', 'Au Quartier GÃ©nÃ©ral, le centre de contrÃ´le situÃ© dans la tÃªte de la petite Riley, 11 ans, cinq Ã‰motions sont au travail. Lorsque la famille de Riley emmÃ©nage dans une grande ville, avec tout ce que cela peut avoir dâ€™effrayant, les Ã‰motions ont fort Ã  faire pour guider la jeune fille durant cette difficile transition.', 'Francais', 'Comï¿½die', '20/10/15 - 21H44', 'VICEVERSA.jpg'),
(13, 'La Rage au ventre', 'La Rage au ventre', '2015', 'Antoine Fuqua', '5', 'Champion du monde de boxe, Billy Hope mÃ¨ne une existence fastueuse avec sa superbe femme et sa fille quâ€™il aime plus que tout. Lorsque sa femme est tuÃ©e, son monde sâ€™Ã©croule. Il va devoir se battre pour trouver la voie de la rÃ©demption et regagner ainsi la garde de sa fille.', 'Anglais', 'Action', '20/10/15 - 21H46', 'RAGE.jpg'),
(14, 'Straight Outta Compton', 'Straight Outta Compton', '2015', 'F. Gary Gray', '9', 'En 1987, cinq jeunes hommes exprimaient leur frustration et leur colÃ¨re pour dÃ©noncer les conditions de vie de l''endroit le plus dangereux de lâ€™AmÃ©rique avec l''arme la plus puissante qu''ils possÃ©daient : leur musique.', 'Anglais', 'Action', '20/10/15 - 21H49', 'COMP.jpg'),
(15, 'Kingsman', 'Kingsman', '2015', 'Matthew Vaughn', '6', 'KINGSMAN, lâ€™Ã©lite du renseignement britannique en costumes trois piÃ¨ces, est Ã  la recherche de sang neuf. Pour recruter leur nouvel agent secret, elle doit faire subir un entrainement de haut vol Ã  de jeunes privilÃ©giÃ©s aspirant au job rÃªvÃ©.', 'Anglais', 'Action', '20/10/15 - 21H51', 'KING.jpg'),
(16, 'Mad Max: Fury Road', 'Mad Max: Fury Road', '2015', 'George Miller', '7', 'HantÃ© par un lourd passÃ©, Mad Max estime que le meilleur moyen de survivre est de rester seul. Cependant, il se retrouve embarquÃ© par une bande qui parcourt la DÃ©solation Ã  bord d''un vÃ©hicule militaire pilotÃ© par l''Imperator Furiosa.', 'Espagnol', 'Action', '20/10/15 - 22H10', 'MAD.jpg'),
(17, 'Forrest Gump', 'Forrest Gump', '1994', 'Robert Zemeckis', '9', 'Quelques dÃ©cennies d''histoire amÃ©ricaine, des annÃ©es 1940 Ã  la fin du XXÃ¨me siÃ¨cle, Ã  travers le regard et l''Ã©trange odyssÃ©e d''un homme simple et pur, Forrest Gump.', 'Francais', 'Aventures', '20/10/15 - 22H11', 'GUMP.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
