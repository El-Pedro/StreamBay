Lien: http://localhost/~inizan/StreamBay/index.php?page=movie

Project PHP 2A3: StreamBay
Members: Pierre-Adrien INIZAN, Nicolas PAULIN, Quentin KREMER

Subject:
On veut realiser une application de gestion d’une collection de films en PHP. Cette application ne demande pas
d’authentification mais doit permettre de:
	— Consulter la liste des films disponibles selon differents criteres
	— Gerer les titres, realisateurs, annees de realisation et genre (policier, comedie, dessin anime, etc.)
	— Ajouter un film
	— Supprimer un film
On considerera qu’un film a un seul realisateur et un seul genre. Nourrissez votre base avec une liste de films
recuperes sur le Web. Inutile de prendre une base trop importante. Options : gerer la modification des films et les affiches de films, qu’on recuperera eventuellement aussi sur le Web. Examinez de pres cette demande et donnez en une analyse et une impl ementation SQL pour commencer.

Fonctionnalitees:
- Consultation des films selon different criteres (images cover films + titre)
	- Details
- Rechercher films par titreVF (JQuery)
- Ajout films dans le BDD
- Suppression de films

TODO:
- Ajout films:
	- verifications cote client (JQuery)
	- verification cote serveur (PHP)
- Edit de films