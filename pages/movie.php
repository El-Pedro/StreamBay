<div id="addLightbox">
	<script type="text/javascript" src="js/addFormControl.js"></script>
	<div id="close">×</div>
	<h2>Ajouter un film:</h2>
	<form name="addLightbox" method="POST">
		<?php
			if(isset($_POST['submit'])){
				//tous les infos de la bdd 
				$titreVO = htmlentities($_POST['titreVO']);
				$titreFR = htmlentities($_POST['titreFR']);
				$annee = htmlentities($_POST['annee']);
				$realisateur = htmlentities($_POST['realisateur']);
				$note = htmlentities($_POST['note']);
				$desc = htmlspecialchars($_POST['desc']);
				$lang = htmlentities($_POST['lang']);
				$genre = htmlentities($_POST['genre']);
				
				if(!empty($_POST['coverimg'])){
					$image = $_POST["coverimg"];
				}else{
					$image = "nose.png";
				}
				
				addMoviesDB($db,$titreVO,$titreFR,$annee,$realisateur,$note,$desc,$lang,$genre,$image);
			}
		?>
		<table>
			<tr>
				<td class="w150px">Titre Original:</td>
				<td>
					<input type="text" name="titreVO" id="titreVO">
				</td>
				<td >
					<span class='error_titreVO' style='position:relative;color:red;font-weight:bold;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Titre Francais:</td>
				<td>
					<input type="text" name="titreFR" id="titreFR">
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Annee Realisation:</td>
				<td>
					<select name="annee" id="annee">
						<?php
							for($i=2016;$i>=1900;$i--){
								echo "<option value=".$i.">".$i."</option>";
							}
						?>
					</select>
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Realisateur:</td>
				<td>
					<input type="text" name="realisateur" id="realisateur">
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Note:</td>
				<td>
					<select name="note" id="note">
					<?php
						for($i=10;$i>=0;$i--){
							echo "<option value=".$i.">".$i."</option>";
						}
					?>
				</select>
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Description:</td>
				<td>
					<textarea style="height:80px;" id="desc" name="desc"></textarea>
				</td>
			</tr>
			<tr>
				<td class="w150px">Langue:</td>
				<td>
					<select name="lang">
						<option value="Anglais">Anglais</option>
						<option value="Francais">Francais</option>
						<option value="Espagnol">Espagnol</option>
						<option value="Italien">Italien</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="w150px">Genre:</td>
				<td>
					<?php displayGenre($db); ?>
				</td>
			</tr>
			<tr>
				<td class="w150px">Image:</td>
				<td>
					<input type="file" name="coverimg" id="coverimg">
				</td>
			</tr>
		</table>
		<input type="submit" name="submit" class="submit" id="submit" value="Ajouter">
	</form>
</div>

<div id="infoMovie"></div>
<div id="films">
	<form method="POST" name="triFilm">
		<h4>Trier par:</h4> <select name="tri">
			<option value='tous'>Tous</option>
			<option value='note'>Note</option>
			<option value='dernier'>Derniers ajoute</option>
			<option value='genre'>Genre</option>
		</select>
		<input type="submit" name="val" id="val" value="Trier" class="submit">
	</form>
	<?php
		if(isset($_POST['val'])){
			if($_POST['tri'] == 'note'){
				?>
					<h5>Trié par note:</h5>
					<ul><?php displayFilms($db,"SELECT * FROM movies ORDER BY note DESC"); ?></ul>
				<?php
			}elseif($_POST['tri'] == 'genre'){
				?>
					<h5>Trié par genre:</h5>
					<ul><?php displayFilms($db,"SELECT * FROM movies ORDER BY genre ASC"); ?></ul>
				<?php
			}
			elseif($_POST['tri'] == 'tous'){
				?>
					<h5>Liste de tous les films:</h5>
					<ul><?php displayFilms($db,"SELECT * FROM movies ORDER BY titreVF"); ?></ul>
				<?php
			}elseif($_POST['tri'] == 'dernier'){
				?>
					<h5>7 derniers films ajouté:</h5>
					<ul><?php displayDernierFilms($db); ?></ul>
				<?php
			}
		}else{
			?>
				<h5>Liste de tous les films:</h5>
				<ul><?php displayFilms($db,"SELECT * FROM movies ORDER BY titreVF"); ?></ul>
			<?php
		}
	?>
</div>