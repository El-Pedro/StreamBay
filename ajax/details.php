<?php
	include('../config.php');
	if(isset($_POST['id'])){
		?>
			<div id="close2">×</div>
			<div id="feedback"></div>
		<?php $id = $_POST['id']; ?>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#close2').click(function(){
					$('#infoMovie').fadeOut();
					$('#infoMovie2').fadeOut();
				});
				$('#close2').click(function(){
					$('#infoMovie2').fadeOut();
				});

				$('#delete').click(function(){
					var id = '<?php echo $id; ?>';
					$.post('ajax/remove.php',{id:id},function(data){
				        location.reload();
				    });
				});
			});
		</script> 

		<?php
		$query = $db->query("SELECT * FROM movies WHERE idF = '$id'");
		while ($data = $query->fetch()){
			?>	
				<div id="contentMovie">
					<h2><?php echo $data['titreVF']; ?></h2>
					<img src="images/cover/<?php echo $data['image']; ?>" width="150" height="200" id="popupCover">
					<table>
						<tr>
							<div id="tab">
								<u><b>Date de sortie:</u></b> 
								<?php echo $data['dateRea']; ?>
							</div>
						</tr>
						<tr>
							<div id="tab">
								<u><b>Note:</u></b> 
								<?php echo $data['note']."/10"; ?>
							</div>
						</tr>
						<tr>
							<div id="tab">
								<u><b>Realisateur:</u></b>
								<?php echo $data['realisateur']; ?>
							</div>
						</tr>
						<tr>
							<div id="tab">
								<u><b>Langue:</u></b>
								<?php echo $data['lang']; ?>
							</div>
						</tr>
						<tr>
							<div id="tab">
								<u><b>Genre:</u></b>
								<?php echo $data['genre']; ?>
							</div>
						</tr>
						<tr>
							<div id="tab">
								<u><b>Date Ajout:</u></b>
								<?php echo $data['dateAjout']; ?>
							</div>
						</tr>
					</table>
					<div id="coverdesc">
						<u><b>Description:</u></b>
						<?php echo $data['description']; ?>
					</div>
					<button type='submit' id='edit' class="submit">Editer</button>
					<button type='submit' id='delete' class="submit">Supprimer</button>
				</div>
			<?php
		}

	}
?>
