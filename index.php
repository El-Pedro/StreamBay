<?php
	$page = htmlentities($_GET['page']);
	$pages = scandir('pages');

	include('config.php');
	include('functions/'.$page.'.func.php');

	if(!empty($page) && in_array($_GET['page'].".php",$pages)){
		$content = 'pages/'.$_GET['page'].".php";
	}else{
		header("Location:index.php?page=movie");
	}
?>
<html>
	<head>
		<title>Popcorn Time</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="icon" type="image/png" href="images/logo.png" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	</head>
	<body>
		<div id="content">
			<header>
				<img id="logo" src="images/logo.png">
				<h1>StreamBay</h1>
			</header>
			<div id="query">
				<?php 
					include("pages/menu.php"); 
					include($content);
				?>
			</div>
		</div>
	</body>
</html>