<?php
	//la fonction qui affiche tous les films
	function displayFilms($db,$sql){
		$query = $db->query($sql);
		while ($data = $query->fetch()){
			$id = $data['idF'];
		?>	
			<li onclick=
			"
			var id = '<?php echo $id; ?>';
			$.post('ajax/details.php',{id:id},function(data){
		        $('#infoMovie').html(data);
		    });
			" 
					id="film<?php echo $id; ?>">
				<img width="150" height="200" src="images/cover/<?php echo $data['image']; ?>">
		    	<div id="titreFilm">
		    		<?php echo $data['titreVF']; ?>
		   		 </div>
			</li>
		<?php
		}
	}

	//la fonction qui affiche les 7 derniers films ajoute dans la db
	function displayDernierFilms($db){
		$query = $db->query("SELECT * FROM movies ORDER BY idF DESC");
		$cpt = 0;
		while ($data = $query->fetch()){
			$cpt++;
			if($cpt <= 7){
				$id = $data['idF'];
				?>	
				<li onclick=
				"
				var id = '<?php echo $id; ?>';
				$.post('ajax/details.php',{id:id},function(data){
				    $('#infoMovie').html(data);
				});
				" 
				id="film<?php echo $id; ?>">
				<img width="150" height="200" src="images/cover/<?php echo $data['image']; ?>">
				<div id="titreFilm">
					<?php echo $data['titreVF']; ?>
				</div>
				</li>
				<?php
			}
		}
	}

	//la fonction qui ajoute un film dans la bdd
	function addMoviesDB($db,$titreVO,$titreFR,$annee,$realisateur,$note,$desc,$lang,$genre,$image){
		$dateAjout = date('d/m/y').' - '.date('H')."H".date('i');
		$query = $db->prepare('INSERT INTO movies(titreVO,titreVF,dateRea,realisateur,note,description,lang,genre,dateAjout,image) 
						  VALUES(:titreVO,:titreVF,:dateRea,:realisateur,:note,:description,:lang,:genre,:dateAjout,:image)');
		$query->execute(array(
			'titreVO'     => $titreVO,
			'titreVF'     => $titreFR,
			'dateRea'     => $annee,
			'realisateur' => $realisateur,
			'note'        => $note,
			'description' => $desc,
			'lang'        => $lang,
			'genre'       => $genre,
			'dateAjout'   => $dateAjout,
			'image'      => $image
		));
	}

	//la fonction qui affiche toute la liste des genres
	function displayGenre($db){
		echo "<select name='genre' id='genre'>";
		$query = $db->query('SELECT * FROM genres ORDER BY nom_genre');

		while ($data = $query->fetch()){
		    echo "<option value=".$data['nom_genre'].">".$data['nom_genre']."</option>";
		}
		echo "</select>";
	}
?>